-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2021 at 07:13 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurant_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `image` varchar(2052) NOT NULL,
  `description` text NOT NULL,
  `posted_by` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `title`, `image`, `description`, `posted_by`) VALUES
(52, 'Rizoto  $25', '1615982053rizoto.jpg', 'rice,  mushroom sauce, boiled vegetables\r\n', 'ana'),
(53, 'Pizza $30', '1615982131piza.jpg', 'bacon, sauce, cheese, mushrooms', 'ana'),
(54, 'Salad 25$', '1615982152salad.jpg', 'tomatoes, cucumbers, onions, cheese, salt', 'ana'),
(55, 'Cake 20$', '1615982175cake.jpg', 'chocolate, strawberries, pudding', 'ana'),
(56, 'Ice   $10', '1615982263ice.jpg', 'strawberries, chocolate, banana', 'ana'),
(57, 'Orange juic    $10', '1615982304drink.jpg', 'oranges, carrots, and sugar', 'ana'),
(58, 'strawberries  $10', '1615982327drinnk.jpg', 'strawberries, carrots and sugar', 'ana'),
(59, 'Pomfrit    $10', '1615982356frit.jpg', 'Pomfrit, cheese', 'ana'),
(60, 'Steak $20', '1615982386stekk.jpg', 'Steak in Scare with vegetables.', 'ana');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(128) NOT NULL,
  `message` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `message`) VALUES
(13, 'Drini', 'drini@gmail.com', 'qqqqqqqqqqqqqqq'),
(14, 'Kushtrim', 'era@gmail.com', '1q1sfsdgxdb'),
(15, 'Kushtrim', 'as44564@ubt-uni.net', 'aaaaaaaaaaaa'),
(18, 'Arjana Selimaj', 'anasallauaka@gmail.com', 'aaaaa aaaa aaaa aaaaaaaaaa.\r\n'),
(19, 'Arjana Selimaj', 'anasallauaka@gmail.com', 'aaaaa aaaa aaaa aaaaaaaaaa.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `staff_info`
--

CREATE TABLE `staff_info` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` varchar(500) NOT NULL,
  `image` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_info`
--

INSERT INTO `staff_info` (`id`, `title`, `description`, `image`) VALUES
(4, 'Menager', 'Laura is the wife of paul, and at\r\nthe same time the manager of the\r\nrestaurant, she is 32 years old, \r\nshe has been working in the restaurant\r\nfor 5 years, she has completed her\r\nuniversity and her master\'s degree \r\nin management ', '1615978641menag.jpg'),
(5, 'Kitchen chef', 'Kelvin</b> is a chef, one of the best\r\nchefs, born and raised in Italy, \r\nhe had a passion for cooking since\r\nhe was a child, Kelvin is 28 years \r\nold, he has completed various\r\ncertifications for chef, and today\r\nhe holds a high position in the restaurant', '1615978698chef.jpg'),
(7, 'Owner', 'Delivery Boys is a 1984 film directed by Ken Handler about a multiethnic group of pizza delivery boys who start a breakdancing team. Mario Van Peebles,', '1615983697ownner.jpg'),
(8, 'Deliver Boy', 'Deliver boy is fast as a rocket Kelvin is a chef, one of the best chefs, born and raised in Italy, he had a passion for cooking since he was a child, Kelvin is 28 years old, he has completed various certifications for chef, and today he holds a high position in the restaurant', '16159837531615983666istockphoto-866198538-612x612.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(144) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_admin`, `created_at`) VALUES
(14, 'Era', 'era@gmail.com', '$2y$10$aR4pghhFm89RVHMjw/bkQ.HctsDjfMkBTrub9GQFu27xLyi0BrXNy', 1, '2021-03-11 11:53:07'),
(16, 'Drin Selimaj', 'drini@gmail.com', '$2y$10$0fL2xvxjk.OtGuRPwfJ1w.jipiOm1uZBiEO8cTg5DvCQWHkL/O7CC', 0, '2021-03-15 12:08:11'),
(17, 'ana', 'ana@gmail.com', '$2y$10$9dFR/b651.ZGPPWiBbF40OUTee7aDU4m.tb/FL16TQHl8ntpcbAxS', 0, '2021-03-16 08:40:21'),
(19, 'albi', 'albi@gmail.com', '$2y$10$nCh7voGkqXj9hdr5eLkIr.wb0Or878A0e3x3.AZPqNvW9UQAxrtWu', 0, '2021-03-16 11:18:03'),
(26, 'Arjana Selimaj', 'era@gmail.com', '$2y$10$7mjvtvuYW7myBE79LjIpUOCIlKX42b4WL34112ohcX4OsPqTtJ1Hy', 0, '2021-03-16 13:01:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_info`
--
ALTER TABLE `staff_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `staff_info`
--
ALTER TABLE `staff_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
